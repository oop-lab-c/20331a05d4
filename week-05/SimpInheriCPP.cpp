#include<iostream>
using namespace std;
class x{
    public:
    void f1(){
        cout<<"this is parent and this is simple inheritance"<<endl;
    }
};
class y : public x{
    public:
    void f(){
        cout<<"this is children "<<endl;
    }
};
int main(){
    y obj;
    obj.f();
    obj.f1();
    return 0;
}