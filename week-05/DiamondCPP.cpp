#include<iostream>
using namespace std;
class Person { //class Person
public:
    Person(int a)  { cout << "The Person is  called" << endl; }
};
 
class Father : public Person { //class Father inherits Person
public:
    Father(int a):Person(a)   {
       cout << "Father is called" << endl;
    }
};
 
class Mother : public Person { //class Mother inherits Person
public:
    Mother(int a):Person(a) {
        cout << "Mother is called" << endl;
    }
};
 
class Child : public Father, public Mother  { //Child inherits Father and Mother
public:
    Child(int a):Mother(a), Father(a)   {
        cout << "Child is called" << endl;
    }
};
 
int main() {
    Child child(20);
}