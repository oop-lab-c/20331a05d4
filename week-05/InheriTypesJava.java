class greatparent
{
    void fungp()
    {
        System.out.println("I am super parent");
    }
}
class parent extends greatparent
{
    void funp()
    {
        System.out.println("I am parent");
    }
}
class child1 extends parent
{
    void child1()
    {
        System.out.println("I am child1");
    }
}
class child2 extends parent
{
    void child2()
    {
        System.out.println("I am child2");
    }
}
class InherTypesJava
{
    public static void main(String[] args)
    {
        System.out.println("this is Simple inheritance");
        parent obj = new parent();
        obj.funp();
        obj.fungp();//single
        System.out.println("This is Multilevel inheritance");
        child2 a = new child2();
        a.fungp();
        a.funp();
        a.child2();//multilevel
        System.out.println("This is Hierarchical inheritance");
        child1 b = new child1();
        b.funp();
        b.child1();//hierachical
    }
}
