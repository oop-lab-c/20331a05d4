class Parent
{
    void mesg()
    {
        System.out.println("parent class ");
    }
}
class SimpleInherJava extends Parent
{
    void mesg1()
    {
        System.out.println("child class ");
    }
    public static void main(String args[])
    {
        SimpleInherJava obj = new SimpleInherJava();
        obj.mesg();
        obj.mesg1();
    }
}
