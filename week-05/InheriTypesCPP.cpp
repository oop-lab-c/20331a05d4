//17 week5
#include<iostream>
using namespace std;
class par1
{
    public:
    void f()
    {
        cout<<"grand parent1 is me"<<endl;
    }
};
class par2
{
    public:
    void f1()
    {
        cout<<"grandparent2 is me"<<endl;
    }
};
class c : public par1
{
    public:
    void f2()
    {
        cout<<"This is simple inheritance"<<endl;
    }
};
class d :public par1,par2
{
    public:
    void f3()
    {
        cout<<"This is multiple inheritance"<<endl;
    }
};
class e :public c
{
    public:
    void f4()
    {
        cout<<"This is multilevel inhreitance"<<endl;
    }
};
class g :public c
{
    public:
    void f5()
    {
        cout<<"and This is hierarchical inheritance"<<endl;
    }
};
int main()
{
    c obj ;
    obj.f2();
    obj.f();
    d obj1 ;
    obj1.f();
    obj1.f3();
    e obj2;
    obj2.f();
    obj2.f2();
    obj2.f4();
    g obj3;
    obj3.f();
    obj3.f2();
    obj3.f5();

}
