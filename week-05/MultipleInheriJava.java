interface a
{
    void fun();
}
interface b
{
    void dis();
}
class  c implements a,b 
{
    public void fun()
    {
        System.out.println("good night");
    }
    public void dis()
    {
        System.out.println("sweet dreams");
    }
}
class MultipleInheriJava
{
    public static void main(String[] args)
    {
        c obj = new c();
        obj.dis();
        obj.fun();
    }
}