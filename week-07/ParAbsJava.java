abstract class ParAbsJava{
    abstract void display();
}
class hide extends ParAbsJava
{
    void display()
    {
        System.out.println("By using abstract classes we can achieve impure abstarction.");
    }
    public static void main(String[] args)
    {
        hide obj = new hide();
        obj.display();
    }
}