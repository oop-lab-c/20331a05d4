class x{
    void fun(int x, int y){
        System.out.println(x+y);
    }
}
class MethodOLInheriJava extends x{
    void fun(int x, int y, int z){
        System.out.println(x+y+z);
    }
    public static void main(String[] args){
        MethodOLInheriJava obj = new MethodOLInheriJava();
        obj.fun(409,195);
        obj.fun(9,56,93);
    }
    
}
