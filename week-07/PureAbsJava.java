interface PureAbsJava
{
    void display();
}
class hide implements PureAbsJava
{
    public void display()
    {
        System.out.println(" By using interfaces only we can achieve pure abstarction .");
    }
    public static void main(String[] args)
    {
        hide obj = new hide();
        obj.display();
    }
}