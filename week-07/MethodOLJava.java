class MethodOLJava
{
    void show(int x, int y)
    {
        System.out.println(x*y);
    }
    void show(int x, int y,int z)
    {
        System.out.println(x*y*z);
    }
    public static void main(String[] args)
    {
        MethodOLJava obj = new MethodOLJava();
        obj.show(5,10);
        obj.show(5,10,3);
    }
}