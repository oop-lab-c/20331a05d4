class AbsEncapJava
{
    public int pubvar;
    protected int provar;
    private int privar;
    void setVar(int pubvar,int provar,int privar)
    {
       this.pubvar=pubvar;
       this.provar=provar;
       this.privar=privar;
    }
    void getVar()
    {
        System.out.println(pubvar);
        System.out.println(provar);
        System.out.println(privar);
    }
    public static void main(String[] args)
    {
        AbsEncapJava obj = new AbsEncapJava();
        obj.setVar(31,40,60);
        obj.getVar();
    }
}