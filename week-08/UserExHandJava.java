import java.util.*;  
class UserExHandJava{  
    public static void main(String args[]){  
        try{  
            throw new NewException(9);  
        }  
        catch(NewException ex){  
            System.out.println(ex) ;  
        }
        finally
        {
            System.out.println("\nDone");
        }
    }  
}  
class NewException extends Exception{  
    int a;  
    NewException(int b) {  
        a=b;  
    }  
    public String toString(){  
        return ("The Exception value is = "+a) ;  
    }  
} 