import java.util.*;
class CalcJava
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 1st number : ");
        int n1 = input.nextInt();
        System.out.println("Enter 2nd number : ");
        int n2 = input.nextInt();
        System.out.println("Enter character : ");
        char c = input.next().charAt(0);
        if(c=='+')
        {
            System.out.println(n1+n2);
        }
        else if(c=='-')
        {
            System.out.println(n1-n2);
        }
        else if(c=='*')
        {
            System.out.println(n1*n2);
        }
        else if(c=='/')
        {
            System.out.println(n1/n2);
        }
        else
        {
            System.out.println(n1%n2);
        }

    }
}
